//
//  GoEuroTests.m
//  GoEuroTests
//
//  Created by Artificium on 08/01/14.
//  Copyright (c) 2014 Jacob. All rights reserved.
//

#import "GoEuroTests.h"
#import "GEReachability.h"

@implementation GoEuroTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

/*- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in GoEuroTests");
}*/

- (void)testNetworkConnection
{
    Reachability *r = [Reachability reachabilityWithHostName:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    
    if(internetStatus == NotReachable) {
        STFail(@"Internet connection test failed");
    }
}

@end
