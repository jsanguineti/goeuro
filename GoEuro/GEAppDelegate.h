//
//  GEAppDelegate.h
//  GoEuro
//
//  Created by Artificium on 08/01/14.
//  Copyright (c) 2014 Jacob. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GEViewController;

@interface GEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) GEViewController *viewController;

@end
