//
//  GEViewController.h
//  GoEuro
//
//  Created by Jacopo on 08/01/14.
//  Copyright (c) 2014 Jacob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

/**
 * The setAllowsAnyHTTPSCertificate function allows me to proceed with the request without a proper
 * certificate
 */

@interface NSURLRequest (TestInterface)
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host;
@end

/**
 * The enum CityType allows me to understand which field user is typing
 */
typedef enum {
    kNull,
    kCityFrom,
    kCityTo,
    kDate
} CityType;

@interface GEViewController : UIViewController<CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *cityFromField;
@property (weak, nonatomic) IBOutlet UITextField *cityToField;
@property (weak, nonatomic) IBOutlet UITextField *dateField;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLLocation *myLocation;

/**
 * The responsData contains the original JSON response
 */
@property (nonatomic, strong) NSMutableData *responseData;

/**
 * The arrayValues contains all values
 */
@property (nonatomic, strong) NSArray *arrayValues;

/**
 * The table with the auto-filling values
 */
@property (nonatomic, strong) UITableView *citiesTableView;

/**
 * CityField is relative to the enum CityType
 */
@property (nonatomic) CityType cityField;


/**
 * Toolbar with "done" button to dismiss the DatePicker for the date Field
 */
@property (nonatomic, retain) UIToolbar* toolbarDatePicker;

/**
 * Function with the NSURLConnection
 */
-(void)makeRequest:(NSString*)city;

/**
 * A simple alert dialog just not to repeat over and over the same code
 */
-(void)displayAlert:(NSString*)message;

/**
 * The sortArray function sorts the arrayValues on the base of the distance from my current location
 */
-(void)sortArray;

/**
 * The updateDateField function substitutes the keyboard with a date picker for the date text field
 */
- (void)updateDateField:(id)sender;

/**
 * The removeDatePicker functions removes the date picker for the date text field
 */
- (void)removeDatePicker:(id)sender;

/**
 * The formatDate function formats the date field
 * @return NSString
 */
- (NSString *)formatDate:(NSDate *)date;

/**
 * The textFieldDoneEditing function is relative to "Did end on exit" event for all the text fields
 */
- (IBAction)textFieldDoneEditing:(id)sender;

/**
 * The searchElements function is relative to the "Touch up insdide" event for the search button 
 */
- (IBAction)searchElements:(id)sender;

/**
 * The backgroundTapped function dismisses keyboard/date picker when user touches the view
 */
- (IBAction)backgroundTapped:(id)sender;

@end
