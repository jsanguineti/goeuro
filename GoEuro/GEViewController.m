//
//  GEViewController.m
//  GoEuro
//
//  Created by Artificium on 08/01/14.
//  Copyright (c) 2014 Jacob. All rights reserved.
//

#import "GEViewController.h"

@interface GEViewController ()

@end

@implementation GEViewController

#define SERVER_ADDRESS  @"https://api.goeuro.de/api/v1/suggest/position/en/name/"
#define TOOLBAR_HEIGHT  40

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [_locationManager startUpdatingLocation];
    
    _citiesTableView = [[UITableView alloc] initWithFrame:
                        CGRectMake(20, 100, 280, 120) style:UITableViewStylePlain];
    _citiesTableView.delegate = self;
    _citiesTableView.dataSource = self;
    _citiesTableView.scrollEnabled = YES;
    _citiesTableView.hidden = YES;
     _citiesTableView.backgroundColor = [UIColor colorWithRed:(87/255.0) green:(116/255.0) blue:(128/255.0) alpha:1];
    _citiesTableView.rowHeight = 32.0f;
    [self.view addSubview:_citiesTableView];
    
    _btnSearch.backgroundColor = [UIColor colorWithRed:(254/255.0) green:(165/255.0) blue:(63/255.0) alpha:1];
    [_btnSearch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _cityFromField.textColor = [UIColor colorWithRed:(87/255.0) green:(116/255.0) blue:(128/255.0) alpha:1];
    _cityToField.textColor = [UIColor colorWithRed:(87/255.0) green:(116/255.0) blue:(128/255.0) alpha:1];
    _dateField.textColor = [UIColor colorWithRed:(87/255.0) green:(116/255.0) blue:(128/255.0) alpha:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setBtnSearch:nil];
    [self setDateField:nil];
    [self setCityFromField:nil];
    [self setCityToField:nil];
    [super viewDidUnload];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    _myLocation = newLocation;
    
    if (_myLocation != nil) {
        //NSLog(@"LON: %.8f", _myLocation.coordinate.longitude);
        //NSLog(@"LAT: %.8f", _myLocation.coordinate.latitude);
        
        [_locationManager stopUpdatingLocation];
    }
}

#pragma mark - NSURLConnection and response

- (void)makeRequest:(NSString*)city
{   
    _citiesTableView.hidden = YES;
    self.responseData = [NSMutableData data];
    
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", SERVER_ADDRESS, city]];
    [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    //NSLog(@"didReceiveResponse");
    [self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"didFailWithError");
    //NSLog(@"Connection failed: %@", [error description]);
    
    /*UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Network error. Please retry." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[errorAlert show];*/
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    //NSLog(@"connectionDidFinishLoading");
    //NSLog(@"Succeeded! Received %d bytes of data",[self.responseData length]);
    
    // convert to JSON
    NSError *myError = nil;
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:&myError];
    
    _arrayValues = [res objectForKey:@"results"];
    
    if([_arrayValues count]>0)
    {
        [self sortArray];
        
        CGRect rect;
        
        int wField = _cityFromField.frame.size.width;
        int hField = _cityFromField.frame.size.height;
        int yField;
        
        if(_cityField==kCityFrom)
        {
            yField = _cityFromField.frame.origin.y+hField;
            rect = CGRectMake(20, yField, wField, 120);
        }
        else if(_cityField==kCityTo)
        {
            yField = _cityToField.frame.origin.y+hField;
            rect = CGRectMake(20, yField, wField, 120);
        }
        
        _citiesTableView.frame = rect;
        
        _citiesTableView.hidden = NO;
    }
        
    [self.citiesTableView reloadData];
}

-(void)sortArray
{
    NSArray *order = [_arrayValues sortedArrayUsingComparator:^(id a, id b)
    {
        NSDictionary *arrayA = (NSDictionary*)a;
        NSDictionary *arrayB = (NSDictionary*)b;
        
        NSDictionary *coordsA = [arrayA objectForKey:@"geo_position"];
        NSDictionary *coordsB = [arrayB objectForKey:@"geo_position"];
        
        CGFloat latitudeA = [[coordsA objectForKey:@"latitude"] floatValue];
        CGFloat longitudeA = [[coordsA objectForKey:@"longitude"] floatValue];
        
        CGFloat latitudeB = [[coordsB objectForKey:@"latitude"] floatValue];
        CGFloat longitudeB = [[coordsB objectForKey:@"longitude"] floatValue];
        
        CLLocation *locationA = [[CLLocation alloc] initWithLatitude:latitudeA longitude:longitudeA];
        CLLocation *locationB = [[CLLocation alloc] initWithLatitude:latitudeB longitude:longitudeB];
        
        CLLocationDistance distanceA = [locationA distanceFromLocation:_myLocation];
        CLLocationDistance distanceB = [locationB distanceFromLocation:_myLocation];
        
        if(distanceA<distanceB)
        {
            return NSOrderedAscending;
        }
        else if(distanceA>distanceB)
        {
            return NSOrderedDescending;
        }
        else
        {
            return NSOrderedSame;
        }
    }];
}

-(void)displayAlert:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

- (IBAction)searchElements:(id)sender
{
    if([_cityFromField.text length]==0)
    {
        [self displayAlert:@"The start location field is empty"];
    }
    else if([_cityToField.text length]==0)
    {
        [self displayAlert:@"The end location field is empty"];
    }
    else if([_dateField.text length]==0)
    {
        [self displayAlert:@"The date field is empty"];
    }
    else
    {
        [self displayAlert:@"Search is not yet implemented"];
    }
}

#pragma mark UITextFieldDelegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //self.activeTextField = textField;
    if(textField.tag==kDate)
    {
        // Create a date picker for the date field.
        UIDatePicker *datePicker = [[UIDatePicker alloc]init];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-31536000];
        [datePicker setDate:[NSDate date]];
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        
        // If the date field has focus, display a date picker instead of keyboard.
        // Set the text to the date currently displayed by the picker.
        _dateField.inputView = datePicker;
        _dateField.text = [self formatDate:datePicker.date];
        
        float hDatePicker = datePicker.frame.size.height;
        
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth;
        CGFloat screenHeight;
        
        if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft){
            screenWidth = screenRect.size.height;
            screenHeight = screenRect.size.width;
        }
        else
        {
            screenWidth = screenRect.size.width;
            screenHeight = screenRect.size.height;
        }
        
        //I calculate the toolbar y (20 is for status bar height
        float yDatePicker = screenHeight-hDatePicker-TOOLBAR_HEIGHT-20;
        
        _toolbarDatePicker = [[UIToolbar alloc] initWithFrame:CGRectMake(0, yDatePicker, screenWidth, TOOLBAR_HEIGHT)];
        _toolbarDatePicker.barStyle   = UIBarStyleBlackTranslucent;
        
        UIBarButtonItem *itemDone  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(removeDatePicker:)];
        UIBarButtonItem *itemSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        _toolbarDatePicker.items = @[itemSpace,itemDone];
        [self.view addSubview:_toolbarDatePicker];
        
        [textField becomeFirstResponder];
    }
    else
    {
        //If user is typing the date field and then tap directly on other text field, the toolbar has to be removed
        if([self.view.subviews containsObject:_toolbarDatePicker]) {
            [_toolbarDatePicker removeFromSuperview];
        }
    }
}

- (IBAction)textFieldDoneEditing:(id)sender
{
	[sender resignFirstResponder];
}

- (IBAction)backgroundTapped:(id)sender
{
    [_cityFromField resignFirstResponder];
    [_cityToField resignFirstResponder];
    [_dateField resignFirstResponder];
    
    if([self.view.subviews containsObject:_toolbarDatePicker]) {
        [_toolbarDatePicker removeFromSuperview];
    }
    
    _citiesTableView.hidden = YES;
}

// Called when the date picker changes.
- (void)updateDateField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)_dateField.inputView;
    _dateField.text = [self formatDate:picker.date];
}

// Formats the date chosen with the date picker.
- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"dd'/'MM'/'yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

- (void)removeDatePicker:(id)sender
{
    [_dateField resignFirstResponder];
    [_toolbarDatePicker removeFromSuperview];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //_citiesTableView.hidden = NO;

    if(textField.tag==kCityFrom || textField.tag==kCityTo)
    {
        if([string isEqualToString:@"\n"])
        {
            //NSLog(@"Return key");
        }
        else if([string length]>0)
        {
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            
            _cityField = textField.tag;
            if([substring length]>1)
            {
                [self makeRequest:substring];
            }
        }
    }

    return YES;
}

#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section
{
    return [_arrayValues count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"CityCell";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = [[_arrayValues objectAtIndex:indexPath.row] objectForKey:@"name"];

    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    
    
    if(_cityField==kCityFrom)
    {
        _cityFromField.text = selectedCell.textLabel.text;
    }
    else if(_cityField==kCityTo)
    {
        _cityToField.text = selectedCell.textLabel.text;
    }
    
    _citiesTableView.hidden = YES;
}

@end
