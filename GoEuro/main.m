//
//  main.m
//  GoEuro
//
//  Created by Artificium on 08/01/14.
//  Copyright (c) 2014 Jacob. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GEAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GEAppDelegate class]));
    }
}
